#!/usr/bin/env bash

. /tmp/common.sh

# mysql
pkg_install pwgen
mysql_root_password=$(pwgen -s1 20)
echo "$mysql_root_password" > ~/.mysql_root
sudo debconf-set-selections <<< "mysql-server-5.7 mysql-server/root_password password $mysql_root_password"
sudo debconf-set-selections <<< "mysql-server-5.7 mysql-server/root_password_again password $mysql_root_password"
pkg_install mysql-server
cp -R /vagrant_data/app/fortunecookie-server ~/
(cd ~/fortunecookie-server/resource/; \
    cat *.ddl.sql *.dml.sql *.permissions.sql| mysql -uroot -p`cat ~/.mysql_root`)
