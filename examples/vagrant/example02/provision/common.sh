#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

sudo apt-get update -y 1>/dev/null

pkg_install() {
    package=$1
    echo "installing $1"
    dpkg -s "$package" &>/dev/null || sudo apt install -y "$package"  # 1>/dev/null
}

