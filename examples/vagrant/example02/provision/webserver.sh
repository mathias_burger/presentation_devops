#!/usr/bin/env bash

. /tmp/common.sh

# apache2
pkg_install apache2
sudo a2enmod rewrite
sudo a2enmod proxy
sudo a2enmod proxy_wstunnel
cat <<EOF | sudo tee /etc/apache2/sites-enabled/000-default.conf 1>/dev/null
<VirtualHost *:80>
  ServerName localhost

  RewriteEngine On
  RewriteCond %{HTTP:Upgrade} =websocket [NC]
  RewriteRule /(.*)           ws://localhost:8080/$1 [P,L]

  DocumentRoot /var/www/html
  <Directory>
    Options Indexes FollowSymLinks MultiViews
    AllowOverride None
    Require all granted
  </Directory>
</VirtualHost>
EOF
sudo service apache2 reload

# app
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash - 1>/dev/null
pkg_install nodejs
pkg_install npm
sudo cp -R /vagrant_data/app/fortunecookie-server/src/client /var/www/html
sudo sed -i'' -e 's/localhost:8080/192.168.2.102/' /var/www/html/client/app-client.js
cp -R /vagrant_data/app/fortunecookie-server ~/
cat <<EOF > ~/start_server
#!/usr/bin/env bash
nodejs ~/fortunecookie-server/src/server/app.js
EOF
chmod +x ~/start_server
(cd ~/fortunecookie-server && npm install)
