# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'
settings = YAML.load_file(File.join(__dir__, 'settings.yml'))

Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/yakkety64"

  settings['servers'].each do |servers|
    config.vm.define servers['name'] do |server|
      server.vm.provider :virtualbox do |vb|
        vb.name = servers['name']
        vb.gui = servers['gui']
        vb.memory = settings['ram']

        vb.customize ["modifyvm", :id, "--vram", settings['vram']]
        vb.customize ['modifyvm', :id, '--clipboard', 'bidirectional']

        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      end

      server.vm.hostname = servers['name'] + settings['domain']
      server.vm.network 'private_network', ip: servers['ip'], netmask: settings['netmask']

      server.vm.synced_folder "../../", "/vagrant_data"

      server.vm.provision "file", source: "provision/" + settings['provision'],
                                  destination: "/tmp/" + settings['provision']
      server.vm.provision "shell", path: "provision/" + servers['provision']
    end
  end
end

# TODO DNS https://github.com/oscar-stack/vagrant-hosts
# TODO or vagrant-hostmanager https://github.com/devopsgroup-io/vagrant-hostmanager
