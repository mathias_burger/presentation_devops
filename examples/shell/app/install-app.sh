#!/usr/bin/env bash

# enable "universe" manually in /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive

sudo apt-get update -y 1>/dev/null

pkg_install() {
    package=$1
    echo "installing $1"
    dpkg -s "$package" &>/dev/null || sudo apt install -y "$package"  # 1>/dev/null
}

pkg_install git
pkg_install pwgen

# app
sudo bash nodejs_setup_7.x.sh 1>/dev/null
pkg_install nodejs
pkg_install npm
sudo cp -R ../../app/fortunecookie-server/src/client /var/www/html
cp -R ../../app/fortunecookie-server ~/
cat <<EOF > ~/start_server
#!/usr/bin/env bash
nodejs ~/fortunecookie-server/src/server/app.js
EOF
chmod +x ~/start_server
(cd ~/fortunecookie-server && npm install)

# mysql
mysql_root_password=$(pwgen -s1 20)
echo "$mysql_root_password" > ~/.mysql_root
sudo debconf-set-selections <<< "mysql-server-5.7 mysql-server/root_password password $mysql_root_password"
sudo debconf-set-selections <<< "mysql-server-5.7 mysql-server/root_password_again password $mysql_root_password"
pkg_install mysql-server
(cd ~/fortunecookie-server/resource/; \
    cat *.ddl.sql *.dml.sql *.permissions.sql| mysql -uroot -p`cat ~/.mysql_root`)

# apache2
pkg_install apache2
sudo a2enmod rewrite
sudo a2enmod proxy
sudo a2enmod proxy_wstunnel
cat <<EOF | sudo tee /etc/apache2/sites-enabled/000-default.conf 1>/dev/null
<VirtualHost *:80>
  ServerName localhost

  RewriteEngine On
  RewriteCond %{HTTP:Upgrade} =websocket [NC]
  RewriteRule /(.*)           ws://localhost:8080/$1 [P,L]

  DocumentRoot /var/www/html
  <Directory>
    Options Indexes FollowSymLinks MultiViews
    AllowOverride None
    Require all granted
  </Directory>
</VirtualHost>
EOF
sudo service apache2 reload  # restart is "harmful"
