## Setup

```
npm init

npm install --save ws mysql
```

## Start and test

```
npm src/server/app.js
npm test/integration.js
```

## Mysql

Change root password
```
pwgen -s1 20 > ~/.mysql_root
cat ~/.mysql_root

mysql -uroot
ALTER USER 'root'@'localhost' IDENTIFIED BY '<GENERATED_PASSWORD>';
```
