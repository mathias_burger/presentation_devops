const WebSocket = require('ws');

const wss = new WebSocket.Server({
    perMessageDeflate: false,
    port: 8080
});

wss.on('connection', function connection(ws) {
    ws.on('message', function incoming(message) {
        console.log('received: %s', message);

        if (message === 'tell') {
            fortuneCookieBackend(function (text) {
                ws.send(text);
            });
        }
    });

    ws.send('Welcome to the server!');
});

function fortuneCookieBackend(callback) {
    // you may change this to the local impl when db not yet setup
    // fortuneCookieLocal(callback);
    fortuneCookieDb(callback);
}

function fortuneCookieDb(callback) {
    var result = '';
    const mysql = require('mysql');
    const connection = mysql.createConnection({
        host     : 'localhost',
        user     : 'app-ro',
        password : 'my-app-secret',
        database : 'fortunecookies'
    });

    connection.connect();

    connection.query(
        'SELECT Text FROM fortunecookies ORDER BY RAND() LIMIT 1;',
        function (error, results, fields) {
            if (error) throw error;
            callback(results[0].Text);
        }
    );

    connection.end();
}

function fortuneCookieLocal(callback) {
    var texts = [
        'Stop eating now. Food poisoning no fun.',
        'When chosen for jury duty, tell judge fortune cookie say “guilty!”',
        'You will find a thing. It may be important'
    ];

    callback(texts[random(texts.length)]);
}

function random(upperExclusive) {
    return Math.floor(Math.random() * upperExclusive)
}
