const ws = new WebSocket('ws://localhost:8080');

var displayMessage = function () {},
    tell = function () {};

ws.onopen = function () {
    var firstMsg = true;

    ws.send('Hello, I am a browser!');

    displayMessage = function (text) {
        var pCookie = document.createElement('p');
        pCookie.textContent = text;
        document.getElementById('messages').appendChild(pCookie);
    };

    tell = function () {
        ws.send('tell');
    };

    ws.onmessage = function (e) {
        if (firstMsg) {
            console.log('browser received: %s', e.data);
            firstMsg = false;
        } else {
            displayMessage(e.data);
        }
    };
};

document.getElementById('tell').addEventListener('click', function () { tell(); });
document.getElementById('clear').addEventListener('click', function () {
    document.getElementById('messages').innerText = '';
});
