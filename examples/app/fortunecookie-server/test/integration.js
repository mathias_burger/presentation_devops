const WebSocket = require('ws');

const ws = new WebSocket('ws://localhost:8080', {
    perMessageDeflate: false
});

ws.on('open', function open() {
    ws.send('Hello, I am a client!');

    ws.send('tell');
    ws.send('tell');
    ws.send('tell');
    ws.send('tell');
    ws.send('tell');
});

ws.on('message', function incoming(data, flags) {
    // flags.binary will be set if a binary data is received.
    // flags.masked will be set if the data was masked.
    console.log('client received: %s', data)
});
