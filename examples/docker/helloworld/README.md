# Build docker container

```
docker build --tag mabu/hello .

docker images

docker run --entrypoint=bash -it mabu/hello

docker run -d --name=hello -p11080:80 mabu/hello
```

# Show running containers

```
docker ps
```

# Show output

```
docker logs hello
```

# Remove

```
docker kill hello
docker rm hello
```

# Alpine packages

http://pkgs.alpinelinux.org/packages
