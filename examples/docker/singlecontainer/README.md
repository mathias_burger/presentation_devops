# Build docker container

```
docker build --tag mabu/singlecontainer .

docker images

docker run --entrypoint=bash -it mabu/singlecontainer

docker run -d --name=singlecontainer -p8080:8080 -v $(dirname $(dirname `pwd`))/app:/data mabu/singlecontainer
```

# start stop

```
docker start singlecontainer
docker stop singlecontainer
```

# Show running containers

```
docker ps
```

# Show output

```
docker logs singlecontainer
```

# Remove

```
docker kill hesinglecontainerllo
docker rm singlecontainer
```

# Alpine packages

http://pkgs.alpinelinux.org/packages

# Dockerfile reference

https://docs.docker.com/engine/reference/builder/

# Fortune cookie

Switch to local fortune cookie db instead of mysql to make it work.
