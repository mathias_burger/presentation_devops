# Docker

```
cd docker
```

## Build and run

```
cd docker

docker build --tag mabu/ubuntu .

docker run -d --name=shellprogramming -p 10443:8000 -p 10022:22 mabu/ubuntu

docker stop shellprogramming
docker start shellprogramming
```

## Bash

```
docker run --entrypoint=bash -it mabu/ubuntu
```

## Remove

```
docker kill shellprogramming
docker rm shellprogramming
```

# Gate One

## Style

### Font sizes

Change terminal font size   
```
GateOne.Terminal.loadFont('Ubuntu Mono', '150%');
```